import add from "../wasm/add.wasm";

export async function onRequest() {
	const addModule = await WebAssembly.instantiate(add);
	return new Response("🎉 WASM ROOT says 👉 " + addModule.exports.add(1, 2));
}